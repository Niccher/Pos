/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PosLogic;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.PreparedStatement;
import com.mysql.jdbc.Statement;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileInputStream;
import java.sql.ResultSet;
import java.util.Calendar;
import java.util.GregorianCalendar;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;
import net.proteanit.sql.DbUtils;

/**
 *
 * @author nicch
 */
public class Lista extends javax.swing.JFrame {
    ResultSet rs=null;
    Connection Conn=null;
    PreparedStatement pst=null;
    Statement smt;
    Calendar cal=new GregorianCalendar();
    
    String Patt,Patt2;
    int dt,mnth,year,lst,lst2;

    /**
     * Creates new form Lista
     */
    public Lista() {
        initComponents();
        Conn=(Connection) Infobase.InitDb();
        
        this.setIconImage(new ImageIcon(getClass().getResource("/Res/view.png")).getImage());
        Dimension dim=getToolkit().getScreenSize();
        int jframWidth=this.getSize().width;
        int jframHeight=this.getSize().height;
        int locationX=(dim.width-jframWidth)/2;
        int locationY=(dim.height-jframHeight)/2;
        this.setLocation(locationX, locationY);

        setTitle("Let's Sell 'em");
        Anjia();
    }
    
    private void Anjia(){
        SysDash.setEnabled(Boolean.FALSE);
        LogOff.setEnabled(Boolean.FALSE);
        Welcom.setVisible(Boolean.TRUE);
        NoPrivilege.setVisible(Boolean.FALSE);
        Privileged.setVisible(Boolean.FALSE);
        FieldPop();
        TablPop();
    }
    
    private void Star(){
        //Table Populator
        try {
            String cops = "SELECT `tbl_Stock`.Name,StockDate AS Date,Company,Unit,`tbl_Stock`.StockID,`tbl_StockLevel`.Avail AS Remaining,StockDesc,StockScope,SideEffect FROM `tbl_Stock`,`tbl_StockLevel` WHERE `tbl_Stock`.StockID=`tbl_StockLevel`.StockID";
              pst = ((PreparedStatement)this.Conn.prepareStatement(cops));
              rs = this.pst.executeQuery();
              //StockCurr.setModel(DbUtils.resultSetToTableModel(rs));
              
              String cops1 = "SELECT Name,StockDate AS Date,StockTime AS Time,Company,StockID,StockDesc,SideEffect FROM `tbl_Stock`";
              pst = ((PreparedStatement)this.Conn.prepareStatement(cops1));
              rs = this.pst.executeQuery();
              //StockList.setModel(DbUtils.resultSetToTableModel(rs));
              
              String cops11 = "SELECT Name,StockID AS UniqueID,UnitPrice FROM `tbl_Stock`";
              pst = ((PreparedStatement)this.Conn.prepareStatement(cops11));
              rs = this.pst.executeQuery();
              //tbl_Costos.setModel(DbUtils.resultSetToTableModel(rs));              
              
              String cops2 = "SELECT Name,NationalID,ClientID,Date,Region FROM `tbl_Clients`";
              pst = ((PreparedStatement)this.Conn.prepareStatement(cops2));
              rs = this.pst.executeQuery();
              //ClientList.setModel(DbUtils.resultSetToTableModel(rs));
              
              String cops21 = "SELECT Name,NationalID,ClientID,Date,Region FROM `tbl_Clients`";
              pst = ((PreparedStatement)this.Conn.prepareStatement(cops21));
              rs = this.pst.executeQuery();
              //tbl_ClientEdit.setModel(DbUtils.resultSetToTableModel(rs));
              
              String cops3= "SELECT Name,ClientID,Date,Conditn AS Conditions,Diagnosis,Lab,Drugs FROM `tbl_Treats`";
              pst = ((PreparedStatement)Conn.prepareStatement(cops3));
              rs = pst.executeQuery();
              //TreatsList.setModel(DbUtils.resultSetToTableModel(rs));
              
              
              String cops5= "SELECT Name,NationalID,StaffID,Username,Password,Contacts,Level FROM `tbl_Staff`";
              pst = ((PreparedStatement)Conn.prepareStatement(cops5));
              rs = pst.executeQuery();
              //tbl_Staff.setModel(DbUtils.resultSetToTableModel(rs));
              
              String cops6="SELECT * FROM `tbl_StockLevel` ";
              pst=(PreparedStatement) Conn.prepareStatement(cops6);
              rs=pst.executeQuery();
              //tbl_RfList.setModel(DbUtils.resultSetToTableModel(this.rs));
              
              String cops4= "SELECT * FROM `tbl_Stock`";
              pst = ((PreparedStatement)Conn.prepareStatement(cops4));
              rs = pst.executeQuery();
              //Dr1.removeAllItems();
              /*while (rs.next()) {
                String dr=rs.getString("Name");
                Dr1.addItem(dr);}*/
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e + "\nAction Not Allowed Please Pol");
        }
    }
    
    private void FieldPop(){
        dt=cal.get(Calendar.DAY_OF_MONTH);
        mnth=cal.get(Calendar.MONTH)+1;
        year=cal.get(Calendar.YEAR);
        
        CretStkDate.setEditable(Boolean.FALSE);
        CretStkDate.setEnabled(Boolean.FALSE);
        
        CretUsrDate.setEditable(Boolean.FALSE);
        CretUsrDate.setEnabled(Boolean.FALSE);
        
        CretUsrDate.setText(dt+"-"+mnth+"-"+year);
        CretStkDate.setText(dt+"-"+mnth+"-"+year);
    }
    
    private void TablPop(){
        try {
            String cops11 = "SELECT Name,ID_NO,Reg_No,Date,Contacts,Username,Level FROM `tbl_Users`";
            pst = ((PreparedStatement)this.Conn.prepareStatement(cops11));
            rs = this.pst.executeQuery();
            UserTab.setModel(DbUtils.resultSetToTableModel(rs));
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e + "\nAction Not Allowed Please Pol");
        }
        try {
            String cops11 = "SELECT Count,Name,Stock_ID,Unit,UnitCost FROM `tbl_Stock`";
            pst = ((PreparedStatement)this.Conn.prepareStatement(cops11));
            rs = this.pst.executeQuery();
            StockTab.setModel(DbUtils.resultSetToTableModel(rs));
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e + "\nTable population Failed");
        }
        try {
            String cops11 = "SELECT Count,Name,Stock_ID,Unit,UnitCost,Available FROM `tbl_Stock`";
            pst = ((PreparedStatement)this.Conn.prepareStatement(cops11));
            rs = this.pst.executeQuery();
            StockRefillTab.setModel(DbUtils.resultSetToTableModel(rs));
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e + "\nTable population Failed");
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        Centa = new javax.swing.JPanel();
        NoPrivilege = new javax.swing.JPanel();
        jTabbedPane2 = new javax.swing.JTabbedPane();
        jPanel8 = new javax.swing.JPanel();
        jPanel13 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        SearchCriteria = new javax.swing.JComboBox<>();
        SearchItem = new javax.swing.JTextField();
        SearchAcceptance = new javax.swing.JCheckBox();
        jPanel14 = new javax.swing.JPanel();
        jScrollPane4 = new javax.swing.JScrollPane();
        SearchAccepted = new javax.swing.JTable();
        jLabel4 = new javax.swing.JLabel();
        jTextField8 = new javax.swing.JTextField();
        jLabel15 = new javax.swing.JLabel();
        jTextField14 = new javax.swing.JTextField();
        jButton6 = new javax.swing.JButton();
        jPanel15 = new javax.swing.JPanel();
        jScrollPane5 = new javax.swing.JScrollPane();
        jTable5 = new javax.swing.JTable();
        jPanel9 = new javax.swing.JPanel();
        jScrollPane6 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jLayeredPane1 = new javax.swing.JLayeredPane();
        jLabel16 = new javax.swing.JLabel();
        jTextField5 = new javax.swing.JTextField();
        jLabel14 = new javax.swing.JLabel();
        jTextField4 = new javax.swing.JTextField();
        jTextField3 = new javax.swing.JTextField();
        jTextField7 = new javax.swing.JTextField();
        jLabel41 = new javax.swing.JLabel();
        jTextField1 = new javax.swing.JTextField();
        jLabel25 = new javax.swing.JLabel();
        jLabel43 = new javax.swing.JLabel();
        jLabel42 = new javax.swing.JLabel();
        jTextField2 = new javax.swing.JTextField();
        jButton2 = new javax.swing.JButton();
        jPanel10 = new javax.swing.JPanel();
        Privileged = new javax.swing.JPanel();
        StockPane = new javax.swing.JTabbedPane();
        jPanel1 = new javax.swing.JPanel();
        jPanel5 = new javax.swing.JPanel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        CretUsrName = new javax.swing.JTextField();
        CretUsrID = new javax.swing.JTextField();
        CretUsrDate = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        CretUsrPwd = new javax.swing.JTextField();
        CretUsrUsrname = new javax.swing.JTextField();
        jDesktopPane1 = new javax.swing.JDesktopPane();
        CretUsrImg = new javax.swing.JLabel();
        CretUsrSelIMg = new javax.swing.JTextField();
        CretUsrLv = new javax.swing.JComboBox<>();
        UsrAdd = new javax.swing.JButton();
        UsrEdit = new javax.swing.JButton();
        UsrDel = new javax.swing.JButton();
        jLabel48 = new javax.swing.JLabel();
        CretUsrContact = new javax.swing.JTextField();
        jLabel49 = new javax.swing.JLabel();
        CretUsrReg = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        UserTab = new javax.swing.JTable();
        jPanel2 = new javax.swing.JPanel();
        Refillpane = new javax.swing.JTabbedPane();
        jPanel6 = new javax.swing.JPanel();
        jPanel7 = new javax.swing.JPanel();
        jLabel17 = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        jLabel19 = new javax.swing.JLabel();
        CretStkName = new javax.swing.JTextField();
        CretStkID = new javax.swing.JTextField();
        CretStkDate = new javax.swing.JTextField();
        jLabel20 = new javax.swing.JLabel();
        jLabel21 = new javax.swing.JLabel();
        jLabel22 = new javax.swing.JLabel();
        CretStkUnitPrice = new javax.swing.JTextField();
        CretStkUnit = new javax.swing.JTextField();
        jDesktopPane2 = new javax.swing.JDesktopPane();
        ProdPic = new javax.swing.JLabel();
        jLabel24 = new javax.swing.JLabel();
        jTextField15 = new javax.swing.JTextField();
        CretStkCateg = new javax.swing.JComboBox<>();
        StockAdd = new javax.swing.JButton();
        StockEdit = new javax.swing.JButton();
        StockDel = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        StockTab = new javax.swing.JTable();
        jPanel11 = new javax.swing.JPanel();
        jScrollPane3 = new javax.swing.JScrollPane();
        StockRefillTab = new javax.swing.JTable();
        jLabel35 = new javax.swing.JLabel();
        RefilStkName = new javax.swing.JTextField();
        jLabel36 = new javax.swing.JLabel();
        RefilStkID = new javax.swing.JTextField();
        jLabel37 = new javax.swing.JLabel();
        RefilStkUnit = new javax.swing.JTextField();
        jLabel38 = new javax.swing.JLabel();
        RefilStkUnitPrice = new javax.swing.JTextField();
        StockRefilCommit = new javax.swing.JButton();
        StockUndo = new javax.swing.JButton();
        jLabel39 = new javax.swing.JLabel();
        RefilStkBal = new javax.swing.JTextField();
        jLabel40 = new javax.swing.JLabel();
        RefilStkAdd = new javax.swing.JTextField();
        RefilStkNewBal = new javax.swing.JTextField();
        jPanel3 = new javax.swing.JPanel();
        jPanel16 = new javax.swing.JPanel();
        jScrollPane7 = new javax.swing.JScrollPane();
        jTable2 = new javax.swing.JTable();
        jPanel17 = new javax.swing.JPanel();
        jTextField16 = new javax.swing.JTextField();
        jLabel46 = new javax.swing.JLabel();
        jTextField23 = new javax.swing.JTextField();
        jTextField6 = new javax.swing.JTextField();
        jLabel45 = new javax.swing.JLabel();
        jLabel47 = new javax.swing.JLabel();
        jTextField24 = new javax.swing.JTextField();
        jLabel44 = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        Welcom = new javax.swing.JPanel();
        Infobox = new javax.swing.JPanel();
        jLabel30 = new javax.swing.JLabel();
        jLabel31 = new javax.swing.JLabel();
        jLabel32 = new javax.swing.JLabel();
        jLabel33 = new javax.swing.JLabel();
        jLabel34 = new javax.swing.JLabel();
        AuthBox = new javax.swing.JPanel();
        jLabel26 = new javax.swing.JLabel();
        jLabel27 = new javax.swing.JLabel();
        Pwd = new javax.swing.JPasswordField();
        Usrnm = new javax.swing.JTextField();
        LogLev = new javax.swing.JComboBox<>();
        jLabel28 = new javax.swing.JLabel();
        ProcidLog = new javax.swing.JButton();
        jDesktopPane3 = new javax.swing.JDesktopPane();
        jLabel29 = new javax.swing.JLabel();
        Heda = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        LogOff = new javax.swing.JLabel();
        SysDash = new javax.swing.JLabel();
        Fota = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setMinimumSize(new java.awt.Dimension(1100, 705));
        setPreferredSize(new java.awt.Dimension(1100, 705));
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });

        NoPrivilege.setBackground(new java.awt.Color(204, 204, 255));
        NoPrivilege.setName(""); // NOI18N
        NoPrivilege.setPreferredSize(new java.awt.Dimension(1072, 487));

        jPanel13.setBorder(javax.swing.BorderFactory.createTitledBorder("Search By"));

        jLabel3.setText("Name");

        SearchCriteria.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        SearchAcceptance.setText("Check");

        javax.swing.GroupLayout jPanel13Layout = new javax.swing.GroupLayout(jPanel13);
        jPanel13.setLayout(jPanel13Layout);
        jPanel13Layout.setHorizontalGroup(
            jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel13Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel13Layout.createSequentialGroup()
                        .addComponent(SearchCriteria, javax.swing.GroupLayout.PREFERRED_SIZE, 153, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(SearchAcceptance)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(jPanel13Layout.createSequentialGroup()
                        .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(SearchItem)))
                .addContainerGap())
        );
        jPanel13Layout.setVerticalGroup(
            jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel13Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(SearchItem, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(SearchCriteria, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(SearchAcceptance))
                .addContainerGap())
        );

        jPanel14.setBorder(javax.swing.BorderFactory.createTitledBorder("Buyer's List"));

        SearchAccepted.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane4.setViewportView(SearchAccepted);

        jLabel4.setText("Item Count");

        jLabel15.setText("Total Cost");

        jButton6.setText("Sell");

        javax.swing.GroupLayout jPanel14Layout = new javax.swing.GroupLayout(jPanel14);
        jPanel14.setLayout(jPanel14Layout);
        jPanel14Layout.setHorizontalGroup(
            jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel14Layout.createSequentialGroup()
                .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 3, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel14Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jButton6)
                .addGap(59, 59, 59)
                .addGroup(jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 93, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel15))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jTextField8)
                    .addComponent(jTextField14, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );
        jPanel14Layout.setVerticalGroup(
            jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel14Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 175, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel14Layout.createSequentialGroup()
                        .addGap(17, 17, 17)
                        .addGroup(jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel4)
                            .addComponent(jTextField8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel15)
                            .addComponent(jTextField14, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel14Layout.createSequentialGroup()
                        .addGap(27, 27, 27)
                        .addComponent(jButton6)))
                .addContainerGap(30, Short.MAX_VALUE))
        );

        jPanel15.setBorder(javax.swing.BorderFactory.createTitledBorder("Product List"));

        jTable5.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane5.setViewportView(jTable5);

        javax.swing.GroupLayout jPanel15Layout = new javax.swing.GroupLayout(jPanel15);
        jPanel15.setLayout(jPanel15Layout);
        jPanel15Layout.setHorizontalGroup(
            jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane5, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 655, Short.MAX_VALUE)
        );
        jPanel15Layout.setVerticalGroup(
            jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel15Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, 390, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel8Layout = new javax.swing.GroupLayout(jPanel8);
        jPanel8.setLayout(jPanel8Layout);
        jPanel8Layout.setHorizontalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel14, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPanel13, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel15, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel8Layout.setVerticalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel13, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jPanel14, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 31, Short.MAX_VALUE))
            .addComponent(jPanel15, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        jTabbedPane2.addTab("Sell", jPanel8);

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane6.setViewportView(jTable1);

        jLayeredPane1.setBorder(javax.swing.BorderFactory.createTitledBorder("Insights"));

        jLabel16.setText("jLabel14");

        jLabel14.setText("jLabel14");

        jLabel41.setText("jLabel14");

        jLabel25.setText("jLabel14");

        jLabel43.setText("jLabel14");

        jLabel42.setText("jLabel14");

        jButton2.setText("Refresh");

        jLayeredPane1.setLayer(jLabel16, javax.swing.JLayeredPane.DEFAULT_LAYER);
        jLayeredPane1.setLayer(jTextField5, javax.swing.JLayeredPane.DEFAULT_LAYER);
        jLayeredPane1.setLayer(jLabel14, javax.swing.JLayeredPane.DEFAULT_LAYER);
        jLayeredPane1.setLayer(jTextField4, javax.swing.JLayeredPane.DEFAULT_LAYER);
        jLayeredPane1.setLayer(jTextField3, javax.swing.JLayeredPane.DEFAULT_LAYER);
        jLayeredPane1.setLayer(jTextField7, javax.swing.JLayeredPane.DEFAULT_LAYER);
        jLayeredPane1.setLayer(jLabel41, javax.swing.JLayeredPane.DEFAULT_LAYER);
        jLayeredPane1.setLayer(jTextField1, javax.swing.JLayeredPane.DEFAULT_LAYER);
        jLayeredPane1.setLayer(jLabel25, javax.swing.JLayeredPane.DEFAULT_LAYER);
        jLayeredPane1.setLayer(jLabel43, javax.swing.JLayeredPane.DEFAULT_LAYER);
        jLayeredPane1.setLayer(jLabel42, javax.swing.JLayeredPane.DEFAULT_LAYER);
        jLayeredPane1.setLayer(jTextField2, javax.swing.JLayeredPane.DEFAULT_LAYER);
        jLayeredPane1.setLayer(jButton2, javax.swing.JLayeredPane.DEFAULT_LAYER);

        javax.swing.GroupLayout jLayeredPane1Layout = new javax.swing.GroupLayout(jLayeredPane1);
        jLayeredPane1.setLayout(jLayeredPane1Layout);
        jLayeredPane1Layout.setHorizontalGroup(
            jLayeredPane1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jLayeredPane1Layout.createSequentialGroup()
                .addGap(66, 66, 66)
                .addGroup(jLayeredPane1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jLayeredPane1Layout.createSequentialGroup()
                        .addGroup(jLayeredPane1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(jLayeredPane1Layout.createSequentialGroup()
                                .addComponent(jLabel16, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jTextField2, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jLayeredPane1Layout.createSequentialGroup()
                                .addComponent(jLabel25, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jTextField3, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jLayeredPane1Layout.createSequentialGroup()
                                .addGap(0, 0, Short.MAX_VALUE)
                                .addGroup(jLayeredPane1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jTextField5, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(jLayeredPane1Layout.createSequentialGroup()
                                        .addComponent(jLabel41, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(jTextField4, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                        .addGap(210, 210, 210))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jLayeredPane1Layout.createSequentialGroup()
                        .addGroup(jLayeredPane1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jLayeredPane1Layout.createSequentialGroup()
                                .addComponent(jLabel14, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jLayeredPane1Layout.createSequentialGroup()
                                .addGroup(jLayeredPane1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addComponent(jLabel42, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 107, Short.MAX_VALUE)
                                    .addComponent(jLabel43, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jTextField7, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
            .addGroup(jLayeredPane1Layout.createSequentialGroup()
                .addGap(136, 136, 136)
                .addComponent(jButton2)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        jLayeredPane1Layout.setVerticalGroup(
            jLayeredPane1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jLayeredPane1Layout.createSequentialGroup()
                .addGap(35, 35, 35)
                .addGroup(jLayeredPane1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel14)
                    .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jLayeredPane1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel16)
                    .addComponent(jTextField2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jLayeredPane1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel25)
                    .addComponent(jTextField3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jLayeredPane1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel41)
                    .addComponent(jTextField4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jLayeredPane1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel42)
                    .addComponent(jTextField5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jLayeredPane1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel43)
                    .addComponent(jTextField7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(34, 34, 34)
                .addComponent(jButton2)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel9Layout = new javax.swing.GroupLayout(jPanel9);
        jPanel9.setLayout(jPanel9Layout);
        jPanel9Layout.setHorizontalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel9Layout.createSequentialGroup()
                .addComponent(jScrollPane6, javax.swing.GroupLayout.PREFERRED_SIZE, 633, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLayeredPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        jPanel9Layout.setVerticalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane6, javax.swing.GroupLayout.DEFAULT_SIZE, 460, Short.MAX_VALUE)
            .addComponent(jLayeredPane1, javax.swing.GroupLayout.Alignment.TRAILING)
        );

        jTabbedPane2.addTab("Sales", jPanel9);

        javax.swing.GroupLayout jPanel10Layout = new javax.swing.GroupLayout(jPanel10);
        jPanel10.setLayout(jPanel10Layout);
        jPanel10Layout.setHorizontalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 1143, Short.MAX_VALUE)
        );
        jPanel10Layout.setVerticalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 468, Short.MAX_VALUE)
        );

        jTabbedPane2.addTab("More", jPanel10);

        javax.swing.GroupLayout NoPrivilegeLayout = new javax.swing.GroupLayout(NoPrivilege);
        NoPrivilege.setLayout(NoPrivilegeLayout);
        NoPrivilegeLayout.setHorizontalGroup(
            NoPrivilegeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTabbedPane2)
        );
        NoPrivilegeLayout.setVerticalGroup(
            NoPrivilegeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTabbedPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 487, Short.MAX_VALUE)
        );

        Privileged.setName(""); // NOI18N
        Privileged.setPreferredSize(new java.awt.Dimension(1072, 487));

        StockPane.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                StockPaneMouseClicked(evt);
            }
        });

        jLabel8.setText("Name");

        jLabel9.setText("ID");

        jLabel10.setText("Date");

        jLabel11.setText("Level");

        jLabel12.setText("Username");

        jLabel13.setText("Password");

        CretUsrImg.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                CretUsrImgMouseClicked(evt);
            }
        });

        jDesktopPane1.setLayer(CretUsrImg, javax.swing.JLayeredPane.DEFAULT_LAYER);

        javax.swing.GroupLayout jDesktopPane1Layout = new javax.swing.GroupLayout(jDesktopPane1);
        jDesktopPane1.setLayout(jDesktopPane1Layout);
        jDesktopPane1Layout.setHorizontalGroup(
            jDesktopPane1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jDesktopPane1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(CretUsrImg, javax.swing.GroupLayout.DEFAULT_SIZE, 316, Short.MAX_VALUE)
                .addContainerGap())
        );
        jDesktopPane1Layout.setVerticalGroup(
            jDesktopPane1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDesktopPane1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(CretUsrImg, javax.swing.GroupLayout.PREFERRED_SIZE, 280, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(22, Short.MAX_VALUE))
        );

        CretUsrSelIMg.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                CretUsrSelIMgMouseClicked(evt);
            }
        });

        CretUsrLv.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        UsrAdd.setText("Add");
        UsrAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                UsrAddActionPerformed(evt);
            }
        });

        UsrEdit.setText("Edit");
        UsrEdit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                UsrEditActionPerformed(evt);
            }
        });

        UsrDel.setText("Delete");
        UsrDel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                UsrDelActionPerformed(evt);
            }
        });

        jLabel48.setText("Contact");

        CretUsrContact.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CretUsrContactActionPerformed(evt);
            }
        });

        jLabel49.setText("Reg No.");

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel11)
                            .addComponent(jLabel12)
                            .addComponent(jLabel13))
                        .addGap(30, 30, 30)
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(CretUsrUsrname)
                            .addComponent(CretUsrID)
                            .addComponent(CretUsrDate)
                            .addComponent(CretUsrPwd)
                            .addComponent(CretUsrName, javax.swing.GroupLayout.PREFERRED_SIZE, 190, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(CretUsrLv, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 339, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel5Layout.createSequentialGroup()
                                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel8)
                                    .addComponent(jLabel9)
                                    .addComponent(jLabel10))
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addGroup(jPanel5Layout.createSequentialGroup()
                                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel48)
                                    .addComponent(jLabel49))
                                .addGap(47, 47, 47)
                                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(CretUsrContact, javax.swing.GroupLayout.DEFAULT_SIZE, 145, Short.MAX_VALUE)
                                    .addComponent(CretUsrReg))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 44, Short.MAX_VALUE)))
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jDesktopPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(CretUsrSelIMg, javax.swing.GroupLayout.PREFERRED_SIZE, 300, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap())
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addGap(40, 40, 40)
                .addComponent(UsrAdd)
                .addGap(62, 62, 62)
                .addComponent(UsrEdit)
                .addGap(74, 74, 74)
                .addComponent(UsrDel)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel8)
                            .addComponent(CretUsrName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(29, 29, 29)
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel9)
                            .addComponent(CretUsrID, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(31, 31, 31)
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel10)
                            .addComponent(CretUsrDate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(27, 27, 27)
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel11)
                            .addComponent(CretUsrLv, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(27, 27, 27)
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel12)
                            .addComponent(CretUsrUsrname, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(31, 31, 31)
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel13)
                            .addComponent(CretUsrPwd, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel48)
                            .addComponent(CretUsrContact, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(jDesktopPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addComponent(CretUsrSelIMg, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel49)
                            .addComponent(CretUsrReg, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(28, 28, 28)))
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(UsrAdd)
                    .addComponent(UsrEdit)
                    .addComponent(UsrDel))
                .addGap(30, 30, 30))
        );

        UserTab.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        UserTab.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                UserTabMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(UserTab);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 406, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 460, Short.MAX_VALUE)
        );

        StockPane.addTab("Users", jPanel1);

        Refillpane.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                RefillpaneMouseClicked(evt);
            }
        });

        jLabel17.setText("Name");

        jLabel18.setText("Product ID");

        jLabel19.setText("Date");

        CretStkName.setToolTipText("");

        CretStkID.setEditable(false);
        CretStkID.setToolTipText("");

        CretStkDate.setToolTipText("");

        jLabel20.setText("Category");

        jLabel21.setText("Unit");

        jLabel22.setText("Unit Price");

        CretStkUnitPrice.setToolTipText("");

        CretStkUnit.setToolTipText("");

        ProdPic.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                ProdPicMouseClicked(evt);
            }
        });

        jDesktopPane2.setLayer(ProdPic, javax.swing.JLayeredPane.DEFAULT_LAYER);

        javax.swing.GroupLayout jDesktopPane2Layout = new javax.swing.GroupLayout(jDesktopPane2);
        jDesktopPane2.setLayout(jDesktopPane2Layout);
        jDesktopPane2Layout.setHorizontalGroup(
            jDesktopPane2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDesktopPane2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(ProdPic, javax.swing.GroupLayout.PREFERRED_SIZE, 276, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jDesktopPane2Layout.setVerticalGroup(
            jDesktopPane2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDesktopPane2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(ProdPic, javax.swing.GroupLayout.DEFAULT_SIZE, 254, Short.MAX_VALUE)
                .addContainerGap())
        );

        jLabel24.setText("jLabel9");

        jTextField15.setToolTipText("");

        CretStkCateg.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        StockAdd.setText("Add");
        StockAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                StockAddActionPerformed(evt);
            }
        });

        StockEdit.setText("Edit");
        StockEdit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                StockEditActionPerformed(evt);
            }
        });

        StockDel.setText("Delete");
        StockDel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                StockDelActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel7Layout.createSequentialGroup()
                        .addComponent(jLabel24)
                        .addGap(38, 38, 38)
                        .addComponent(jTextField15, javax.swing.GroupLayout.PREFERRED_SIZE, 173, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jPanel7Layout.createSequentialGroup()
                        .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel7Layout.createSequentialGroup()
                                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel17)
                                    .addComponent(jLabel19)
                                    .addComponent(jLabel18, javax.swing.GroupLayout.PREFERRED_SIZE, 87, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(CretStkName, javax.swing.GroupLayout.DEFAULT_SIZE, 173, Short.MAX_VALUE)
                                    .addComponent(CretStkDate, javax.swing.GroupLayout.PREFERRED_SIZE, 145, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(CretStkID)))
                            .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addComponent(CretStkUnit, javax.swing.GroupLayout.PREFERRED_SIZE, 145, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGroup(jPanel7Layout.createSequentialGroup()
                                    .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jLabel21)
                                        .addComponent(jLabel22)
                                        .addComponent(jLabel20, javax.swing.GroupLayout.PREFERRED_SIZE, 77, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(jPanel7Layout.createSequentialGroup()
                                            .addGap(22, 22, 22)
                                            .addComponent(CretStkCateg, javax.swing.GroupLayout.PREFERRED_SIZE, 145, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel7Layout.createSequentialGroup()
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                            .addComponent(CretStkUnitPrice, javax.swing.GroupLayout.PREFERRED_SIZE, 145, javax.swing.GroupLayout.PREFERRED_SIZE))))))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jDesktopPane2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(52, 52, 52))))
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addGap(58, 58, 58)
                .addComponent(StockAdd)
                .addGap(44, 44, 44)
                .addComponent(StockEdit)
                .addGap(49, 49, 49)
                .addComponent(StockDel)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel7Layout.createSequentialGroup()
                        .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel17)
                            .addComponent(CretStkName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(29, 29, 29)
                        .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel18)
                            .addComponent(CretStkID, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(31, 31, 31)
                        .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel19)
                            .addComponent(CretStkDate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(27, 27, 27)
                        .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel20)
                            .addComponent(CretStkCateg, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(27, 27, 27)
                        .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel21)
                            .addComponent(CretStkUnit, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(31, 31, 31)
                        .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel22)
                            .addComponent(CretStkUnitPrice, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(jDesktopPane2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(13, 13, 13)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel24)
                    .addComponent(jTextField15, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(41, 41, 41)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(StockAdd)
                    .addComponent(StockEdit)
                    .addComponent(StockDel))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        StockTab.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        StockTab.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                StockTabMouseClicked(evt);
            }
        });
        jScrollPane2.setViewportView(StockTab);

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, 599, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 457, Short.MAX_VALUE))
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 433, Short.MAX_VALUE)
        );

        Refillpane.addTab("Register", jPanel6);

        StockRefillTab.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        StockRefillTab.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                StockRefillTabMouseClicked(evt);
            }
        });
        jScrollPane3.setViewportView(StockRefillTab);

        jLabel35.setText("Name");

        RefilStkName.setToolTipText("");

        jLabel36.setText("Product ID");

        RefilStkID.setToolTipText("");

        jLabel37.setText("Unit");

        RefilStkUnit.setToolTipText("");

        jLabel38.setText("Unit Price");

        RefilStkUnitPrice.setToolTipText("");

        StockRefilCommit.setText("Commit");
        StockRefilCommit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                StockRefilCommitActionPerformed(evt);
            }
        });

        StockUndo.setText("Undo");

        jLabel39.setText("Available");

        RefilStkBal.setToolTipText("");

        jLabel40.setText("Add");

        RefilStkAdd.setToolTipText("");
        RefilStkAdd.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                RefilStkAddKeyTyped(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                RefilStkAddKeyReleased(evt);
            }
        });

        javax.swing.GroupLayout jPanel11Layout = new javax.swing.GroupLayout(jPanel11);
        jPanel11.setLayout(jPanel11Layout);
        jPanel11Layout.setHorizontalGroup(
            jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel11Layout.createSequentialGroup()
                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel11Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel11Layout.createSequentialGroup()
                                .addComponent(jLabel38)
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addGroup(jPanel11Layout.createSequentialGroup()
                                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel39)
                                    .addGroup(jPanel11Layout.createSequentialGroup()
                                        .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jLabel35)
                                            .addComponent(jLabel37)
                                            .addComponent(jLabel36, javax.swing.GroupLayout.PREFERRED_SIZE, 77, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                            .addComponent(RefilStkUnitPrice, javax.swing.GroupLayout.DEFAULT_SIZE, 200, Short.MAX_VALUE)
                                            .addComponent(RefilStkUnit)
                                            .addComponent(RefilStkID)
                                            .addComponent(RefilStkName)))
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel11Layout.createSequentialGroup()
                                        .addComponent(jLabel40)
                                        .addGap(59, 59, 59)
                                        .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                            .addGroup(jPanel11Layout.createSequentialGroup()
                                                .addComponent(RefilStkAdd, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(18, 18, 18)
                                                .addComponent(RefilStkNewBal))
                                            .addComponent(RefilStkBal, javax.swing.GroupLayout.PREFERRED_SIZE, 203, javax.swing.GroupLayout.PREFERRED_SIZE))))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                    .addGroup(jPanel11Layout.createSequentialGroup()
                        .addGap(50, 50, 50)
                        .addComponent(StockRefilCommit)
                        .addGap(38, 38, 38)
                        .addComponent(StockUndo)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 742, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        jPanel11Layout.setVerticalGroup(
            jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 433, Short.MAX_VALUE)
            .addGroup(jPanel11Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel35)
                    .addComponent(RefilStkName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(29, 29, 29)
                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel36)
                    .addComponent(RefilStkID, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(31, 31, 31)
                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel37)
                    .addComponent(RefilStkUnit, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(30, 30, 30)
                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel38)
                    .addComponent(RefilStkUnitPrice, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(30, 30, 30)
                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel39)
                    .addComponent(RefilStkBal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(34, 34, 34)
                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel40)
                    .addComponent(RefilStkAdd, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(RefilStkNewBal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(48, 48, 48)
                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(StockRefilCommit)
                    .addComponent(StockUndo))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        Refillpane.addTab("Refill", jPanel11);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(Refillpane)
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(Refillpane)
        );

        StockPane.addTab("Stock", jPanel2);

        jPanel16.setBorder(javax.swing.BorderFactory.createTitledBorder("Sales"));

        jTable2.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane7.setViewportView(jTable2);

        jPanel17.setBorder(javax.swing.BorderFactory.createTitledBorder("Sales Margins"));

        jLabel46.setText("jLabel44");

        jLabel45.setText("jLabel45");

        jLabel47.setText("jLabel45");

        jLabel44.setText("jLabel44");

        jButton1.setText("Refresh");

        javax.swing.GroupLayout jPanel17Layout = new javax.swing.GroupLayout(jPanel17);
        jPanel17.setLayout(jPanel17Layout);
        jPanel17Layout.setHorizontalGroup(
            jPanel17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel17Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel17Layout.createSequentialGroup()
                        .addGroup(jPanel17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(jPanel17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addGroup(jPanel17Layout.createSequentialGroup()
                                    .addComponent(jLabel44)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                    .addComponent(jTextField6, javax.swing.GroupLayout.PREFERRED_SIZE, 155, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(jPanel17Layout.createSequentialGroup()
                                    .addComponent(jLabel45)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                    .addComponent(jTextField16, javax.swing.GroupLayout.PREFERRED_SIZE, 155, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(jPanel17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addGroup(jPanel17Layout.createSequentialGroup()
                                    .addComponent(jLabel46)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                    .addComponent(jTextField23, javax.swing.GroupLayout.PREFERRED_SIZE, 155, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(jPanel17Layout.createSequentialGroup()
                                    .addComponent(jLabel47)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                    .addComponent(jTextField24, javax.swing.GroupLayout.PREFERRED_SIZE, 155, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addContainerGap())
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel17Layout.createSequentialGroup()
                        .addComponent(jButton1)
                        .addGap(58, 58, 58))))
        );
        jPanel17Layout.setVerticalGroup(
            jPanel17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel17Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel44)
                    .addComponent(jTextField6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel45)
                    .addComponent(jTextField16, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel46)
                    .addComponent(jTextField23, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel47)
                    .addComponent(jTextField24, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jButton1)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel16Layout = new javax.swing.GroupLayout(jPanel16);
        jPanel16.setLayout(jPanel16Layout);
        jPanel16Layout.setHorizontalGroup(
            jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel16Layout.createSequentialGroup()
                .addComponent(jScrollPane7, javax.swing.GroupLayout.DEFAULT_SIZE, 773, Short.MAX_VALUE)
                .addGap(18, 18, 18)
                .addComponent(jPanel17, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        jPanel16Layout.setVerticalGroup(
            jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane7, javax.swing.GroupLayout.DEFAULT_SIZE, 438, Short.MAX_VALUE)
            .addComponent(jPanel17, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel16, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel16, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        StockPane.addTab("Finances", jPanel3);

        javax.swing.GroupLayout PrivilegedLayout = new javax.swing.GroupLayout(Privileged);
        Privileged.setLayout(PrivilegedLayout);
        PrivilegedLayout.setHorizontalGroup(
            PrivilegedLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(StockPane)
        );
        PrivilegedLayout.setVerticalGroup(
            PrivilegedLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(StockPane)
        );

        StockPane.getAccessibleContext().setAccessibleName("Users");

        Welcom.setPreferredSize(new java.awt.Dimension(1072, 487));

        Infobox.setBackground(new java.awt.Color(204, 204, 204));

        jLabel30.setText("jLabel30");

        jLabel31.setText("jLabel31");

        jLabel32.setText("jLabel32");

        jLabel33.setText("jLabel33");

        jLabel34.setText("jLabel34");

        javax.swing.GroupLayout InfoboxLayout = new javax.swing.GroupLayout(Infobox);
        Infobox.setLayout(InfoboxLayout);
        InfoboxLayout.setHorizontalGroup(
            InfoboxLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(InfoboxLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(InfoboxLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel30)
                    .addComponent(jLabel31)
                    .addComponent(jLabel32)
                    .addComponent(jLabel33)
                    .addComponent(jLabel34))
                .addContainerGap(359, Short.MAX_VALUE))
        );
        InfoboxLayout.setVerticalGroup(
            InfoboxLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(InfoboxLayout.createSequentialGroup()
                .addGap(70, 70, 70)
                .addComponent(jLabel30)
                .addGap(37, 37, 37)
                .addComponent(jLabel31)
                .addGap(41, 41, 41)
                .addComponent(jLabel32)
                .addGap(35, 35, 35)
                .addComponent(jLabel33)
                .addGap(37, 37, 37)
                .addComponent(jLabel34)
                .addContainerGap(192, Short.MAX_VALUE))
        );

        AuthBox.setBackground(new java.awt.Color(204, 204, 255));

        jLabel26.setText("Username");

        jLabel27.setText("Password");

        Pwd.setToolTipText("");

        Usrnm.setToolTipText("");

        LogLev.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "<-Select->", "Seller", "Admin" }));

        jLabel28.setText("Login as");

        ProcidLog.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Res/go-next-5.png"))); // NOI18N
        ProcidLog.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ProcidLogActionPerformed(evt);
            }
        });

        jLabel29.setText("jLabel29");

        jDesktopPane3.setLayer(jLabel29, javax.swing.JLayeredPane.DEFAULT_LAYER);

        javax.swing.GroupLayout jDesktopPane3Layout = new javax.swing.GroupLayout(jDesktopPane3);
        jDesktopPane3.setLayout(jDesktopPane3Layout);
        jDesktopPane3Layout.setHorizontalGroup(
            jDesktopPane3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDesktopPane3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel29, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        jDesktopPane3Layout.setVerticalGroup(
            jDesktopPane3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDesktopPane3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel29, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        javax.swing.GroupLayout AuthBoxLayout = new javax.swing.GroupLayout(AuthBox);
        AuthBox.setLayout(AuthBoxLayout);
        AuthBoxLayout.setHorizontalGroup(
            AuthBoxLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, AuthBoxLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jDesktopPane3)
                .addGap(33, 33, 33)
                .addGroup(AuthBoxLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(ProcidLog, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel28, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(LogLev, javax.swing.GroupLayout.PREFERRED_SIZE, 165, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(33, 33, 33))
            .addGroup(AuthBoxLayout.createSequentialGroup()
                .addGap(89, 89, 89)
                .addGroup(AuthBoxLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel26)
                    .addComponent(jLabel27))
                .addGap(46, 46, 46)
                .addGroup(AuthBoxLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(Usrnm)
                    .addComponent(Pwd, javax.swing.GroupLayout.PREFERRED_SIZE, 209, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(246, Short.MAX_VALUE))
        );
        AuthBoxLayout.setVerticalGroup(
            AuthBoxLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, AuthBoxLayout.createSequentialGroup()
                .addGroup(AuthBoxLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(AuthBoxLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jDesktopPane3)
                        .addGap(18, 18, 18))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, AuthBoxLayout.createSequentialGroup()
                        .addContainerGap(224, Short.MAX_VALUE)
                        .addComponent(jLabel28)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(LogLev, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(50, 50, 50)))
                .addGroup(AuthBoxLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(Usrnm, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel26))
                .addGap(28, 28, 28)
                .addGroup(AuthBoxLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(Pwd, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel27))
                .addGap(7, 7, 7)
                .addComponent(ProcidLog, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(51, 51, 51))
        );

        javax.swing.GroupLayout WelcomLayout = new javax.swing.GroupLayout(Welcom);
        Welcom.setLayout(WelcomLayout);
        WelcomLayout.setHorizontalGroup(
            WelcomLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 1141, Short.MAX_VALUE)
            .addGroup(WelcomLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(WelcomLayout.createSequentialGroup()
                    .addComponent(Infobox, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGap(0, 642, Short.MAX_VALUE)))
            .addGroup(WelcomLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, WelcomLayout.createSequentialGroup()
                    .addGap(0, 410, Short.MAX_VALUE)
                    .addComponent(AuthBox, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        );
        WelcomLayout.setVerticalGroup(
            WelcomLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 487, Short.MAX_VALUE)
            .addGroup(WelcomLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(Infobox, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(WelcomLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(AuthBox, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout CentaLayout = new javax.swing.GroupLayout(Centa);
        Centa.setLayout(CentaLayout);
        CentaLayout.setHorizontalGroup(
            CentaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 1143, Short.MAX_VALUE)
            .addGroup(CentaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(CentaLayout.createSequentialGroup()
                    .addGap(0, 0, Short.MAX_VALUE)
                    .addComponent(NoPrivilege, javax.swing.GroupLayout.DEFAULT_SIZE, 1141, Short.MAX_VALUE)
                    .addGap(0, 0, Short.MAX_VALUE)))
            .addGroup(CentaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(CentaLayout.createSequentialGroup()
                    .addGap(0, 0, Short.MAX_VALUE)
                    .addComponent(Privileged, javax.swing.GroupLayout.DEFAULT_SIZE, 1141, Short.MAX_VALUE)
                    .addGap(0, 0, Short.MAX_VALUE)))
            .addGroup(CentaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(CentaLayout.createSequentialGroup()
                    .addGap(0, 0, Short.MAX_VALUE)
                    .addComponent(Welcom, javax.swing.GroupLayout.DEFAULT_SIZE, 1141, Short.MAX_VALUE)
                    .addGap(0, 0, Short.MAX_VALUE)))
        );
        CentaLayout.setVerticalGroup(
            CentaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 487, Short.MAX_VALUE)
            .addGroup(CentaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(CentaLayout.createSequentialGroup()
                    .addGap(0, 0, Short.MAX_VALUE)
                    .addComponent(NoPrivilege, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGap(0, 0, Short.MAX_VALUE)))
            .addGroup(CentaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(CentaLayout.createSequentialGroup()
                    .addGap(0, 0, Short.MAX_VALUE)
                    .addComponent(Privileged, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGap(0, 0, Short.MAX_VALUE)))
            .addGroup(CentaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(CentaLayout.createSequentialGroup()
                    .addGap(0, 0, Short.MAX_VALUE)
                    .addComponent(Welcom, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGap(0, 0, Short.MAX_VALUE)))
        );

        getContentPane().add(Centa, java.awt.BorderLayout.CENTER);

        Heda.setBackground(new java.awt.Color(153, 153, 153));

        jLabel1.setFont(new java.awt.Font("Dialog", 1, 24)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("123");

        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel2.setText("456");

        LogOff.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Res/system-shutdown-3.png"))); // NOI18N
        LogOff.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                LogOffMouseClicked(evt);
            }
        });

        SysDash.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Res/go-home-4.png"))); // NOI18N
        SysDash.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                SysDashMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout HedaLayout = new javax.swing.GroupLayout(Heda);
        Heda.setLayout(HedaLayout);
        HedaLayout.setHorizontalGroup(
            HedaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(HedaLayout.createSequentialGroup()
                .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, 1037, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(LogOff)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(SysDash)
                .addGap(24, 24, 24))
        );
        HedaLayout.setVerticalGroup(
            HedaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(HedaLayout.createSequentialGroup()
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(HedaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(LogOff)
                    .addComponent(SysDash)
                    .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        getContentPane().add(Heda, java.awt.BorderLayout.PAGE_START);

        Fota.setBackground(new java.awt.Color(153, 153, 153));
        Fota.setPreferredSize(new java.awt.Dimension(900, 70));

        jLabel5.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel5.setText("789");

        jLabel6.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel6.setText("101112");

        jLabel7.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel7.setText("131415");

        javax.swing.GroupLayout FotaLayout = new javax.swing.GroupLayout(Fota);
        Fota.setLayout(FotaLayout);
        FotaLayout.setHorizontalGroup(
            FotaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel7, javax.swing.GroupLayout.DEFAULT_SIZE, 1143, Short.MAX_VALUE)
            .addComponent(jLabel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        FotaLayout.setVerticalGroup(
            FotaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(FotaLayout.createSequentialGroup()
                .addComponent(jLabel5)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel6)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 13, Short.MAX_VALUE)
                .addComponent(jLabel7))
        );

        getContentPane().add(Fota, java.awt.BorderLayout.PAGE_END);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void ProcidLogActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ProcidLogActionPerformed
        // TODO add your handling code here:
        String Usr,pwd,lvl;
        Usr=Usrnm.getText().toString();
        pwd=Pwd.getText().toString();
        lvl=LogLev.getSelectedItem().toString();

        String sav="SELECT * FROM `tbl_Users` WHERE `Username`= '"+Usr+"' AND `Password`= '"+pwd+"' "; //AND `Level`= '"+lvl+"'

        try {
            pst=(PreparedStatement) Conn.prepareStatement(sav);
            rs=pst.executeQuery();
            if (rs.next()) {
                SysDash.setEnabled(Boolean.TRUE);
                LogOff.setEnabled(Boolean.TRUE);
                //stats=1;
                if (LogLev.getSelectedIndex()==1) {
                    Welcom.setVisible(Boolean.FALSE);
                    //Infobox.setVisible(Boolean.FALSE);
                    //AuthBox.setVisible(Boolean.FALSE);
                    NoPrivilege.setVisible(Boolean.TRUE);
                    Privileged.setVisible(Boolean.FALSE);
                }
                if (LogLev.getSelectedIndex()==2) {
                    Welcom.setVisible(Boolean.FALSE);
                    //Infobox.setVisible(Boolean.FALSE);
                    //AuthBox.setVisible(Boolean.FALSE);
                    NoPrivilege.setVisible(Boolean.FALSE);
                    Privileged.setVisible(Boolean.TRUE);
                }
            }
        } catch (Exception e) {
            Pwd.setText(null);
            JOptionPane.showMessageDialog(null, "\nInvalid Credentials\n");
        }
        Pwd.setText(null);
    }//GEN-LAST:event_ProcidLogActionPerformed

    private void LogOffMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_LogOffMouseClicked
        // TODO add your handling code here:
        int cl =JOptionPane.showConfirmDialog(this,"Clear This Workspace and Log out \n Unsaved Work WILL Be Lost ","Log Out", JOptionPane.YES_NO_OPTION);
        if (cl==JOptionPane.YES_OPTION) {
            Welcom.setVisible(Boolean.TRUE);
            //Infobox.setVisible(Boolean.FALSE);
            //AuthBox.setVisible(Boolean.FALSE);
            NoPrivilege.setVisible(Boolean.FALSE);
            Privileged.setVisible(Boolean.FALSE);
            
            SysDash.setEnabled(Boolean.FALSE);
            LogOff.setEnabled(Boolean.FALSE);
            
        }
        if (cl==JOptionPane.NO_OPTION) {
            Pwd.setText("");
            Usrnm.setText("");
            //JOptionPane.showMessageDialog(null, "Failed");
            //remove(cl);
        }
    }//GEN-LAST:event_LogOffMouseClicked

    private void SysDashMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_SysDashMouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_SysDashMouseClicked

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        // TODO add your handling code here:
        int cl =JOptionPane.showConfirmDialog(this,"Close This Program \n All Unsaved Work WILL Be Lost","Terminate", JOptionPane.YES_NO_OPTION);
        if (cl==JOptionPane.YES_OPTION) {
            System.exit(0);
        }/*else{
            return;
        }*/
        if (cl==JOptionPane.NO_OPTION) {
            remove(cl);
        }
    }//GEN-LAST:event_formWindowClosing

    private void UsrAddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_UsrAddActionPerformed
        // TODO add your handling code here:
        try {
            String Kret="INSERT INTO `tbl_Users` (`Count`,`Name` ,`ID_NO` , `Reg_No` ,`Date` ,`Level` ,`Username` ,`Password`,`Contacts` ,`Avatar` ) VALUES (NULL,?,?,?,?,?,?,?,?,?)";
            File img=new File(Patt);
            FileInputStream fis=new FileInputStream(img);
            int len=(int)img.length();
            pst= (PreparedStatement) Conn.prepareStatement(Kret);

            pst.setString(1, CretUsrName.getText().toString());
            pst.setInt(2, Integer.parseInt(CretUsrID.getText().toString()));
            pst.setInt(3, Integer.parseInt(CretUsrReg.getText().toString()));
            pst.setString(4, CretUsrDate.getText().toString());
            pst.setString(5, CretUsrLv.getSelectedItem().toString());
            pst.setString(6, CretUsrUsrname.getText().toString());
            pst.setString(7, CretUsrPwd.getText().toString());
            pst.setInt(8, Integer.parseInt(CretUsrContact.getText().toString()));
            pst.setBinaryStream(9,fis, len);

            pst.executeUpdate();            

            JOptionPane.showMessageDialog(null, "New User ( "+CretUsrName.getText().toString()+" )Succesfully Inserted");

        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex+"\nRegistering New User Encounterd An Error");
            Toolkit.getDefaultToolkit().beep();
        }
        TablPop();
    }//GEN-LAST:event_UsrAddActionPerformed

    private void UsrEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_UsrEditActionPerformed
        // TODO add your handling code here:
        try {
            int ro=UserTab.getSelectedRow();
            String cc=UserTab.getModel().getValueAt(ro, 2).toString();
            
            String UserNM,UserLV,UserUSR,UserPWD;
            UserNM= CretUsrName.getText().toString();
            UserLV= CretUsrLv.getSelectedItem().toString();
            UserUSR= CretUsrUsrname.getText().toString();
            UserPWD= CretUsrPwd.getText().toString();
            int UserCONT= Integer.parseInt(CretUsrContact.getText().toString());
            
            String sav="UPDATE `tbl_Users` SET `Name` = '"+UserNM+"',`Level` ='"+UserLV+"',Username = '"+UserUSR+"',Password = '"+UserPWD+"' , Contacts = '"+UserCONT+"' WHERE `Reg_NO`= '"+Integer.parseInt(CretUsrReg.getText().toString())+"' ";
            
            pst=(PreparedStatement) Conn.prepareStatement(sav);
            pst.execute();
            JOptionPane.showMessageDialog(null, "Saved");
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error"+e);
        }
        TablPop();
    }//GEN-LAST:event_UsrEditActionPerformed

    private void UsrDelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_UsrDelActionPerformed
        // TODO add your handling code here:
        try {
            int ro=UserTab.getSelectedRow();
            String cc=UserTab.getModel().getValueAt(ro, 2).toString();
            String sav="DELETE FROM `tbl_Users` WHERE `Reg_No`= ' "+cc+" '";
            pst=(PreparedStatement) Conn.prepareStatement(sav);
            pst.execute();
            
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "\nCleint Deletion Error"+ e);
        }
        TablPop();
    }//GEN-LAST:event_UsrDelActionPerformed

    private void CretUsrSelIMgMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_CretUsrSelIMgMouseClicked
        // TODO add your handling code here:
        try {
            JFileChooser prip=new JFileChooser();
            FileFilter flft=new FileNameExtensionFilter("Images Only", new String []{"jpg","png","jpeg"});

            prip.setFileFilter(flft);
            prip.addChoosableFileFilter(flft);
            int rtn=prip.showOpenDialog(null);
            
            if (rtn==JFileChooser.APPROVE_OPTION) {
                File f= new File(prip.getSelectedFile().getAbsolutePath());//prip.getSelectedFile();
                Patt=f.getAbsolutePath();
                CretUsrSelIMg.setText(Patt);

                ImageIcon stV=new ImageIcon(Patt);
                Image Sd=stV.getImage().getScaledInstance(CretUsrImg.getWidth(),CretUsrImg.getHeight(), Image.SCALE_SMOOTH);
                ImageIcon im=new ImageIcon(Sd);
                CretUsrImg.setIcon(im);
            }

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Null or Invalid 'Image' File");
        }
    }//GEN-LAST:event_CretUsrSelIMgMouseClicked

    private void StockRefilCommitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_StockRefilCommitActionPerformed
        // TODO add your handling code here:
        try {
            int cc=Integer.valueOf(RefilStkID.getText().toString());
            int cc0=Integer.valueOf(RefilStkAdd.getText().toString());
            String norol="UPDATE `tbl_Stock` SET `Available`='"+cc0+"' WHERE `Stock_ID`=' "+cc+" '";
            pst=(PreparedStatement) Conn.prepareStatement(norol);
            pst.executeUpdate();
            TablPop();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "\nRefill Has Encountered An error"+ e);
        }
    }//GEN-LAST:event_StockRefilCommitActionPerformed

    private void StockDelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_StockDelActionPerformed
        // TODO add your handling code here:
        /*try {
            int ro=StockList.getSelectedRow();
            String cc=StockList.getModel().getValueAt(ro, 2).toString();
            String sav="DELETE FROM `tbl_Clients` WHERE `ClientID`= ' "+cc+" '";
            pst=(PreparedStatement) Conn.prepareStatement(sav);
            pst.execute();
            
            //RepoPoplt();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "\nCleint Deletion Error");
        }*/
    }//GEN-LAST:event_StockDelActionPerformed

    private void StockEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_StockEditActionPerformed
        // TODO add your handling code here:
        /*try {
            int ro=StockTab.getSelectedRow();
            String cc=StockTab.getModel().getValueAt(ro, 2).toString();
            
            String snm = CretStkName.getText().toString();
            String scdt = CretStkName.getText().toString();
            String sctm = CretStkName.getText().toString();
            String scom = CretStkName.getText().toString();
            String scun = CretStkName.getText().toString();
            
            String sav="UPDATE `tbl_Clients` SET `Name` = '"+snm+"',`NationalID` ='"+scdt+"',Contacts = '"+scom+"', Region = '"+scun+"' WHERE `ClientID`= '"+Integer.parseInt(ClientID1.getText().toString())+"' ";
            
            pst=(PreparedStatement) Conn.prepareStatement(sav);
            pst.execute();

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "\nClient Update Has Been Unsuccesfull");
        }*/
        
        TablPop();
    }//GEN-LAST:event_StockEditActionPerformed

    private void StockAddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_StockAddActionPerformed
        // TODO add your handling code here:
        try {
                String lv="INSERT INTO `tbl_Stock` ( `Count`,`Name`,`Stock_ID`,`Unit`,`UnitCost`,`Avatar` ) VALUES (NULL,?,?,?,?,?)";
                pst=(PreparedStatement) Conn.prepareStatement(lv);
                
                File img=new File(Patt2);
                FileInputStream fis=new FileInputStream(img);
                int len=(int)img.length();
                
                pst.setString(1, CretStkName.getText().toString());
                pst.setInt(2, Integer.parseInt(CretStkID.getText().toString()));
                pst.setInt(3, Integer.parseInt(CretStkUnit.getText().toString()));
                pst.setInt(4, Integer.parseInt(CretStkUnitPrice.getText().toString()));
                pst.setBinaryStream(5,fis, len);
                
                pst.executeUpdate();
                JOptionPane.showMessageDialog(null, "Succesfull inserted");

        } catch (Exception e) {
                JOptionPane.showMessageDialog(null, "\n"+e);
                Toolkit.getDefaultToolkit().beep();
        }
        TablPop();
    }//GEN-LAST:event_StockAddActionPerformed

    private void CretUsrImgMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_CretUsrImgMouseClicked
        // TODO add your handling code here:
        try {
            JFileChooser prip=new JFileChooser();
            FileFilter flft=new FileNameExtensionFilter("Images Only", new String []{"jpg","png","jpeg"});

            prip.setFileFilter(flft);
            prip.addChoosableFileFilter(flft);
            int rtn=prip.showOpenDialog(null);
            
            if (rtn==JFileChooser.APPROVE_OPTION) {
                File f= new File(prip.getSelectedFile().getAbsolutePath());//prip.getSelectedFile();
                Patt=f.getAbsolutePath();
                CretUsrSelIMg.setText(Patt);

                ImageIcon stV=new ImageIcon(Patt);
                Image Sd=stV.getImage().getScaledInstance(CretUsrImg.getWidth(),CretUsrImg.getHeight(), Image.SCALE_SMOOTH);
                ImageIcon im=new ImageIcon(Sd);
                CretUsrImg.setIcon(im);
            }

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Null or Invalid 'Image' File");
        }
    }//GEN-LAST:event_CretUsrImgMouseClicked

    private void CretUsrContactActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CretUsrContactActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_CretUsrContactActionPerformed

    private void UserTabMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_UserTabMouseClicked
        // TODO add your handling code here:
        try {
            int ro=UserTab.getSelectedRow();
            String otrow=UserTab.getModel().getValueAt(ro, 2).toString();
            int rws=Integer.parseInt(otrow);
            String sav="SELECT * FROM `tbl_Users` WHERE `Reg_No`= '"+rws+"' ";
            pst=(PreparedStatement) Conn.prepareStatement(sav);
            rs=pst.executeQuery();
            if (rs.next()) {
                String ssns,ssid,ssdt,sstm,sscm,sssc,ssdc,ssun,ssef;
                ssns=rs.getString("Name");
                CretUsrName.setText(ssns);
                ssid=rs.getString("ID_NO");
                CretUsrID.setText(ssid);
                ssef=rs.getString("Reg_No");
                CretUsrReg.setText(ssef);
                sstm=rs.getString("Username");
                ssun=rs.getString("Password");
                sssc=rs.getString("Contacts");
                ssdc=rs.getString("Level");
                CretUsrUsrname.setText(sstm);
                CretUsrContact.setText(sssc);
                CretUsrPwd.setText(ssun);
                CretUsrLv.setSelectedItem(ssdc);
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "\nAn Error Occured While Processing Data");
        }
    }//GEN-LAST:event_UserTabMouseClicked

    private void ProdPicMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_ProdPicMouseClicked
        // TODO add your handling code here:
        try {
            JFileChooser prip=new JFileChooser();
            FileFilter flft=new FileNameExtensionFilter("Images Only", new String []{"jpg","png","jpeg"});

            prip.setFileFilter(flft);
            prip.addChoosableFileFilter(flft);
            int rtn=prip.showOpenDialog(null);
            
            if (rtn==JFileChooser.APPROVE_OPTION) {
                File f= new File(prip.getSelectedFile().getAbsolutePath());//prip.getSelectedFile();
                Patt2=f.getAbsolutePath();
                //CretUsrSelIMg.setText(Patt);

                ImageIcon stV=new ImageIcon(Patt2);
                Image Sd=stV.getImage().getScaledInstance(ProdPic.getWidth(),ProdPic.getHeight(), Image.SCALE_SMOOTH);
                ImageIcon im=new ImageIcon(Sd);
                ProdPic.setIcon(im);
            }

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Null or Invalid 'Image' File");
        }
    }//GEN-LAST:event_ProdPicMouseClicked

    private void StockPaneMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_StockPaneMouseClicked
        // TODO add your handling code here:
        try {
            String sql="SELECT * FROM `tbl_Stock`  ORDER BY `Count` DESC LIMIT 1";
            pst= (PreparedStatement) Conn.prepareStatement(sql);
            rs=pst.executeQuery();
            if (rs.next()) {
                lst=rs.getInt("Stock_ID");
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e+"\nStock Progressing Error");
            //Toolkit.getDefaultToolkit().beep();
        }
        CretStkID.setText(String.valueOf(lst+1));
        CretStkID.setEnabled(Boolean.FALSE);
        TablPop();
    }//GEN-LAST:event_StockPaneMouseClicked

    private void RefillpaneMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_RefillpaneMouseClicked
        // TODO add your handling code here:
        RefilStkName.setEnabled(Boolean.FALSE);
        RefilStkID.setEnabled(Boolean.FALSE);
        RefilStkUnit.setEnabled(Boolean.FALSE);
        RefilStkUnitPrice.setEnabled(Boolean.FALSE);
        RefilStkBal.setEnabled(Boolean.FALSE);
        RefilStkNewBal.setEnabled(Boolean.FALSE);
        TablPop();
    }//GEN-LAST:event_RefillpaneMouseClicked

    private void RefilStkAddKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_RefilStkAddKeyTyped
        // TODO add your handling code here:
        char cc=evt.getKeyChar();
        if(!(Character.isDigit(cc) || cc==KeyEvent.VK_BACK_SPACE || cc==KeyEvent.VK_DELETE )){
            getToolkit().beep();
            evt.consume();
        }
    }//GEN-LAST:event_RefilStkAddKeyTyped

    private void RefilStkAddKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_RefilStkAddKeyReleased
        // TODO add your handling code here:
        try {
            String sql="SELECT * FROM `tbl_Stock`  ORDER BY `Count` DESC LIMIT 1";
            pst= (PreparedStatement) Conn.prepareStatement(sql);
            rs=pst.executeQuery();
            if (rs.next()) {
                lst2=rs.getInt("Available");
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e+"\nStock LevelMAnipulation Error");;
        }
        int kk=Integer.parseInt(RefilStkAdd.getText().toString());
        int hh= lst2+kk;
        RefilStkNewBal.setText(String.valueOf(hh));
    }//GEN-LAST:event_RefilStkAddKeyReleased

    private void StockRefillTabMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_StockRefillTabMouseClicked
        // TODO add your handling code here:
        try {
            int ro=StockRefillTab.getSelectedRow();
            String otrow=StockRefillTab.getModel().getValueAt(ro, 2).toString();
            int rws=Integer.parseInt(otrow);
            String sav="SELECT * FROM `tbl_Stock` WHERE `Stock_ID`= '"+rws+"' ";
            pst=(PreparedStatement) Conn.prepareStatement(sav);
            rs=pst.executeQuery();
            if (rs.next()) {
                String ssns,ssid,ssdt,sstm,sscm,sssc,ssdc,ssun,ssef;
                ssns=rs.getString("Name");
                RefilStkName.setText(ssns);
                ssid=rs.getString("Stock_ID");
                RefilStkID.setText(ssid);
                ssef=rs.getString("Unit");
                RefilStkUnit.setText(ssef);
                sstm=rs.getString("UnitCost");
                ssun=rs.getString("Available");
                RefilStkUnitPrice.setText(sstm);
                RefilStkBal.setText(ssun);
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "\nAn Error Occured While Processing Data");
        }
    }//GEN-LAST:event_StockRefillTabMouseClicked

    private void StockTabMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_StockTabMouseClicked
        // TODO add your handling code here:
        try {
            int ro=StockTab.getSelectedRow();
            String otrow=StockTab.getModel().getValueAt(ro, 2).toString();
            int rws=Integer.parseInt(otrow);
            String sav="SELECT * FROM `tbl_Stock` WHERE `Stock_ID`= '"+rws+"' ";
            pst=(PreparedStatement) Conn.prepareStatement(sav);
            rs=pst.executeQuery();
            if (rs.next()) {
                String ssns,ssid,ssdt,sstm,sscm,sssc,ssdc,ssun,ssef;
                ssns=rs.getString("Name");
                CretStkName.setText(ssns);
                ssid=rs.getString("Stock_ID");
                CretStkID.setText(ssid);
                ssef=rs.getString("Unit");
                CretStkUnit.setText(ssef);
                sstm=rs.getString("UnitCost");
                CretStkUnitPrice.setText(sstm);
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "\nAn Error Occured While Processing Data");
        }
    }//GEN-LAST:event_StockTabMouseClicked

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Lista.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Lista.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Lista.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Lista.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Lista().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel AuthBox;
    private javax.swing.JPanel Centa;
    private javax.swing.JComboBox<String> CretStkCateg;
    private javax.swing.JTextField CretStkDate;
    private javax.swing.JTextField CretStkID;
    private javax.swing.JTextField CretStkName;
    private javax.swing.JTextField CretStkUnit;
    private javax.swing.JTextField CretStkUnitPrice;
    private javax.swing.JTextField CretUsrContact;
    private javax.swing.JTextField CretUsrDate;
    private javax.swing.JTextField CretUsrID;
    private javax.swing.JLabel CretUsrImg;
    private javax.swing.JComboBox<String> CretUsrLv;
    private javax.swing.JTextField CretUsrName;
    private javax.swing.JTextField CretUsrPwd;
    private javax.swing.JTextField CretUsrReg;
    private javax.swing.JTextField CretUsrSelIMg;
    private javax.swing.JTextField CretUsrUsrname;
    private javax.swing.JPanel Fota;
    private javax.swing.JPanel Heda;
    private javax.swing.JPanel Infobox;
    private javax.swing.JComboBox<String> LogLev;
    private javax.swing.JLabel LogOff;
    private javax.swing.JPanel NoPrivilege;
    private javax.swing.JPanel Privileged;
    private javax.swing.JButton ProcidLog;
    private javax.swing.JLabel ProdPic;
    private javax.swing.JPasswordField Pwd;
    private javax.swing.JTextField RefilStkAdd;
    private javax.swing.JTextField RefilStkBal;
    private javax.swing.JTextField RefilStkID;
    private javax.swing.JTextField RefilStkName;
    private javax.swing.JTextField RefilStkNewBal;
    private javax.swing.JTextField RefilStkUnit;
    private javax.swing.JTextField RefilStkUnitPrice;
    private javax.swing.JTabbedPane Refillpane;
    private javax.swing.JCheckBox SearchAcceptance;
    private javax.swing.JTable SearchAccepted;
    private javax.swing.JComboBox<String> SearchCriteria;
    private javax.swing.JTextField SearchItem;
    private javax.swing.JButton StockAdd;
    private javax.swing.JButton StockDel;
    private javax.swing.JButton StockEdit;
    private javax.swing.JTabbedPane StockPane;
    private javax.swing.JButton StockRefilCommit;
    private javax.swing.JTable StockRefillTab;
    private javax.swing.JTable StockTab;
    private javax.swing.JButton StockUndo;
    private javax.swing.JLabel SysDash;
    private javax.swing.JTable UserTab;
    private javax.swing.JButton UsrAdd;
    private javax.swing.JButton UsrDel;
    private javax.swing.JButton UsrEdit;
    private javax.swing.JTextField Usrnm;
    private javax.swing.JPanel Welcom;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton6;
    private javax.swing.JDesktopPane jDesktopPane1;
    private javax.swing.JDesktopPane jDesktopPane2;
    private javax.swing.JDesktopPane jDesktopPane3;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel28;
    private javax.swing.JLabel jLabel29;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel30;
    private javax.swing.JLabel jLabel31;
    private javax.swing.JLabel jLabel32;
    private javax.swing.JLabel jLabel33;
    private javax.swing.JLabel jLabel34;
    private javax.swing.JLabel jLabel35;
    private javax.swing.JLabel jLabel36;
    private javax.swing.JLabel jLabel37;
    private javax.swing.JLabel jLabel38;
    private javax.swing.JLabel jLabel39;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel40;
    private javax.swing.JLabel jLabel41;
    private javax.swing.JLabel jLabel42;
    private javax.swing.JLabel jLabel43;
    private javax.swing.JLabel jLabel44;
    private javax.swing.JLabel jLabel45;
    private javax.swing.JLabel jLabel46;
    private javax.swing.JLabel jLabel47;
    private javax.swing.JLabel jLabel48;
    private javax.swing.JLabel jLabel49;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JLayeredPane jLayeredPane1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel10;
    private javax.swing.JPanel jPanel11;
    private javax.swing.JPanel jPanel13;
    private javax.swing.JPanel jPanel14;
    private javax.swing.JPanel jPanel15;
    private javax.swing.JPanel jPanel16;
    private javax.swing.JPanel jPanel17;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JScrollPane jScrollPane7;
    private javax.swing.JTabbedPane jTabbedPane2;
    private javax.swing.JTable jTable1;
    private javax.swing.JTable jTable2;
    private javax.swing.JTable jTable5;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JTextField jTextField14;
    private javax.swing.JTextField jTextField15;
    private javax.swing.JTextField jTextField16;
    private javax.swing.JTextField jTextField2;
    private javax.swing.JTextField jTextField23;
    private javax.swing.JTextField jTextField24;
    private javax.swing.JTextField jTextField3;
    private javax.swing.JTextField jTextField4;
    private javax.swing.JTextField jTextField5;
    private javax.swing.JTextField jTextField6;
    private javax.swing.JTextField jTextField7;
    private javax.swing.JTextField jTextField8;
    // End of variables declaration//GEN-END:variables
}
