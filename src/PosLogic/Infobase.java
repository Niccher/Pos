/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PosLogic;

import com.mysql.jdbc.Driver;
import java.sql.*;
import java.util.Locale;
import javax.swing.JOptionPane;

/**
 *
 * @author nicch
 */
public class Infobase {
    private Connection conn=null;
    
    public static Connection InitDb() {
            try{
                Class.forName("com.mysql.jdbc.Driver");
                Connection conn=DriverManager.getConnection("jdbc:mysql://localhost:3306/","root","");

                PreparedStatement dbs = conn.prepareStatement("CREATE DATABASE IF NOT EXISTS `Hardwire`");//Hardwire
                dbs.execute();

               Statement stt = conn.createStatement();
               stt.execute("USE Hardwire");
               
               String Std = "CREATE TABLE IF NOT EXISTS `tbl_Users` (`Count` INT AUTO_INCREMENT UNIQUE, `Name` VARCHAR(10) NOT NULL ,`ID_NO` INT(10) NOT NULL , `Reg_No` INT(6) NOT NULL PRIMARY KEY , `Date` VARCHAR(10) NOT NULL , `Contacts` INT(12) NOT NULL , `Username` VARCHAR(10) NOT NULL , `Password` VARCHAR(10) NOT NULL , `Level` VARCHAR(7) NOT NULL , `Avatar` LONGBLOB NOT NULL )";
               PreparedStatement pst1 = conn.prepareStatement(Std);
               pst1.execute();
               
               String Std2 = "CREATE TABLE IF NOT EXISTS `tbl_Stock` (`Count` INT AUTO_INCREMENT UNIQUE, `Name` VARCHAR(10) NOT NULL ,`Stock_ID` INT(10) NOT NULL , `Unit` INT(6) NOT NULL PRIMARY KEY , `UnitCost` VARCHAR(10) NOT NULL , `Available` INT(5) , `Avatar` LONGBLOB NOT NULL )";
               PreparedStatement pst2 = conn.prepareStatement(Std2);
               pst2.execute();
               
               String Std8="INSERT INTO `tbl_Papers` (`Count`, `Subjects`, `Paper_One`, `Paper_Two`, `Paper_Three`) VALUES (NULL, 'Mathematics', 0, 0, 0) ,"+
                        "(NULL, 'English', 0, 0, 0)" +
                        ",(NULL, 'Kiswahili', 0, 0, 0)" +
                        ",(NULL, 'Chemistry', 0, 0, 0)" +
                        ",(NULL, 'Biology', 0, 0, 0)" +
                        ",(NULL, 'Physics', 0, 0, 0)" +
                        ",(NULL, 'Geography', 0, 0, 0)" +
                        ",(NULL, 'CRE', 0, 0, 0)" +
                        ",(NULL, 'Agriculture', 0, 0, 0)" +
                        ",(NULL, 'Business', 0, 0, 0)" ;
                PreparedStatement pst8 = conn.prepareStatement(Std8);
                //pst8.execute();

                String Std14="INSERT INTO `tbl_ClassList` (`Count`, `Class`, `North`, `East`, `West`, `South`) VALUES (NULL, 'Form1', '0', '0', '0', '0'), (NULL, 'Form2', '0', '0', '0', '0'), (NULL, 'Form3', '0', '0', '0', '0'), (NULL, 'Form4', '0', '0', '0', '0')";
                PreparedStatement pst14 = conn.prepareStatement(Std14);
                //pst14.executeUpdate();
                
                Statement stmt=conn.createStatement();
                return conn;

            }catch(Exception ex){
                JOptionPane.showMessageDialog(null,"Database  Error 2");
                return  null;
            }
            //return null;
            
        }
    
    public static void main(String[] args) {
        new Infobase();
    }
    
}
